//
//  DatabaseManager.m
//  ALFirebase
//
//  Created by Philip Broadway on 1/31/15.
//  Copyright (c) 2015 Philip Broadway. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseManager.h"

@implementation DatabaseManager

+ (DatabaseManager *)instance {
    static DatabaseManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[DatabaseManager alloc] init];
        NSString *path = [NSString stringWithFormat:@"https://1422661276551.firebaseio.com/users/%@", _instance.uuid];
        _instance.fb = [[Firebase alloc] initWithUrl:path];
//        [_instance.fb setValue:[NSMutableArray array]];
        
    });
    
    return _instance;
}


- (void)addDictonary:(NSDictionary *)data inNode:(NSString *)nodeName{
    Firebase *nodeReference = [self.fb childByAppendingPath:nodeName];
    Firebase *newChildNode = [nodeReference childByAutoId];
    [newChildNode setValue:data];
}

- (NSString *)uuid {
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}


@end
