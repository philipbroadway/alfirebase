//
//  UsersViewController.m
//  ALFirebase
//
//  Created by Philip Broadway on 1/30/15.
//  Copyright (c) 2015 Philip Broadway. All rights reserved.
//

#import "UsersViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "DatabaseManager.h"


@interface UsersViewController ()
@property NSMutableArray *nodes;
@end

@implementation UsersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nodes = [[NSMutableArray alloc] init];
    
    
    Firebase *randomQuotesNodeReference = [[DatabaseManager instance].fb childByAppendingPath:@"quotes"];
    [randomQuotesNodeReference observeEventType:FEventTypeChildAdded withBlock:^(FDataSnapshot *snapshot) {
        NSLog(@"firebase:%@", snapshot);
        [self.nodes addObject:@{@"quote"    :snapshot.value[@"quote"],
                                @"author"   :snapshot.value[@"author"]
                                }];
        
        [self.tableView reloadData];
        NSLog(@"Total quotes:%lu", (unsigned long)self.nodes.count);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.nodes.count;
}


 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"randomQuoteCell" forIndexPath:indexPath];
     
     NSDictionary *data = [self.nodes objectAtIndex:indexPath.row];
     cell.textLabel.text = data[@"quote"];
     cell.detailTextLabel.text = data[@"author"];
     
    return cell;
 }


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Network

- (void)fetchRandom {
    
    
    
    NSArray *categories = @[@"movies", @"music"];
    NSString *category = [categories objectAtIndex:(arc4random() % [categories count])];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"umfI0QoQcPmshhPdOqqavURZfwksp1ihJiNjsnCX7x5qABfgTP"
                     forHTTPHeaderField:@"X-Mashape-Key"];
    
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager setResponseSerializer:responseSerializer];
    
    
    [manager POST:[NSString stringWithFormat:@"https://andruxnet-random-famous-quotes.p.mashape.com/cat=%@",category]
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"[Received from API]%@", responseObject);
        [[DatabaseManager instance] addDictonary:responseObject inNode:@"quotes"];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        NSLog(@"%@ %@",error, [operation response]);
    }];
    
    
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


#pragma mark - Actions

- (IBAction)addBtnTapped:(id)sender {
    [self fetchRandom];
}
@end
