//
//  AppDelegate.h
//  ALFirebase
//
//  Created by Philip Broadway on 1/31/15.
//  Copyright (c) 2015 Philip Broadway. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

