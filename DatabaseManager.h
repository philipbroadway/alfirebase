//
//  DatabaseManager.h
//  ALFirebase
//
//  Created by Philip Broadway on 1/31/15.
//  Copyright (c) 2015 Philip Broadway. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

@interface DatabaseManager : NSObject

+ (DatabaseManager *)instance;

- (void)addDictonary:(NSDictionary *)data inNode:(NSString *)nodeName;

@property (strong, nonatomic) Firebase *fb;
@property (strong, nonatomic) NSString *uuid;

@end
